import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { CuadroBusquedaComponent } from './components/cuadro-busqueda/cuadro-busqueda.component';

// servicio
import { LiverpoolSearchService  } from './services/liverpool-search.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CuadroBusquedaComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule
  ],
  providers: [LiverpoolSearchService],
  bootstrap: [AppComponent]
})
export class AppModule {}
